#Cloning and running LTP
CWD="$(pwd)"
echo -e "`git clone 'https://github.com/linux-test-project/ltp.git'`\n"
cd ltp
echo -e "`make autotools`"
echo -e "`./configure`"

#Compiling and installing all testcases

echo -e "`make`"
echo -e "`make install`" #This will install LTP to /opt/ltp.

cd $CWD
#LTP integration to RPA

#Step 1: Download the source code from following link (or from git): 

echo -e "`git clone 'https://github.com/robotframework/robotframework.git'`\n" #Installing RPA

#Step 2: If you already have Python 2.7 with pip installed, you can simply run:
echo -e "`pip3 install robotframework`"

#Step 4: After that you can install the framework with:
cd robotframework
echo -e "`sudo python setup.py install`"


cd $CWD
#Creating .robot file

#ltp.robot is created , where each testcases running as RPA tescases and categorised according to categories assuming ltp is installed in /opt/ltp

#Running LTP through RPA
echo -e "`robot u540-ltp.robot`" #it creates log.html, output.xml and report.html

#merging two output files
#Assuming , we are merging output1.xml and output2.xml  for comparing the outputs

#echo -e "`rebot -R --xunit <newfile.xml> output1.xml output2.xml`" # command to merege the files
#<newfile.xml> ,log.html and report.html are new files created after merging




